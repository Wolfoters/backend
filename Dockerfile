FROM phusion/baseimage:latest
RUN apt-get update && apt-get dist-upgrade -y

# NodeJS
RUN apt-get install -y curl && curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs

# PostgreSQL
RUN apt-get install -y postgresql postgresql-client
COPY sysfiles/pg_hba.conf /etc/postgresql/9.6/main/data/pg_hba.conf
RUN service postgresql restart

# Redis
RUN apt-get install -y redis-server

# Slice Backend
WORKDIR /slice
RUN apt-get install -y git
RUN git config --global credential.helper store \
	&& git clone --depth 1 https://gitlab.com/TurboGem/slice/backend.git .

RUN npm i

# SSH
RUN rm -f /etc/service/sshd/down
RUN cat sysfiles/authorized_keys >> /root/.ssh/authorized_keys

# Service
RUN mkdir /etc/service/slice
COPY run.sh /etc/service/slice/run
RUN chmod +x /etc/service/slice/run

ENTRYPOINT [ "/sbin/my_init" ]
