#!/bin/bash

cd /slice

while true; do
	echo -e "\nUpdating.."

	git reset --hard
	git clean -df
	git pull origin master
	
	npm i

	node .

	for second in {3..1}
	do
		echo -ne "Restarting in $second seconds..\r"
		sleep 1
	done
done
